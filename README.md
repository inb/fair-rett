New reference repository for the independent repositories created to contribute to the FAIRification of Rett Syndrome data and analysis of specific trends.

Original repositories are:
- https://github.com/mbosio85/HGVSparse
- https://gitlab.bsc.es/mbosio85/rtt_summary_plot
